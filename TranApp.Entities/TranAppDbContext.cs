﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TranApp.Entities
{
    public partial class TranAppDbContext : DbContext
    {
        //public TranAppDbContext()
        //{
        //}

        public TranAppDbContext(DbContextOptions<TranAppDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbBarang> TbBarang { get; set; }
        public virtual DbSet<TbCustomer> TbCustomer { get; set; }
        public virtual DbSet<TbProduct> TbProduct { get; set; }
        public virtual DbSet<TbPurchase> TbPurchase { get; set; }
        public virtual DbSet<TbTransaction> TbTransaction { get; set; }
        public virtual DbSet<TbUser> TbUser { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseNpgsql("Server=localhost;Database=Db_TranApp;Username=postgres;Password=12345678;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbBarang>(entity =>
            {
                entity.Property(e => e.BarangId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");
            });

            modelBuilder.Entity<TbCustomer>(entity =>
            {
                entity.Property(e => e.CustomerId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");
            });

            modelBuilder.Entity<TbProduct>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("PK_Product");

                entity.Property(e => e.ProductId).UseIdentityAlwaysColumn();
            });

            modelBuilder.Entity<TbPurchase>(entity =>
            {
                entity.HasKey(e => e.PurchaseId)
                    .HasName("PK_Purchase");

                entity.Property(e => e.PurchaseId).UseIdentityAlwaysColumn();

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.TbPurchase)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Purchase_Product");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TbPurchase)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Purchase_User");
            });

            modelBuilder.Entity<TbTransaction>(entity =>
            {
                entity.Property(e => e.TransactionId).UseIdentityAlwaysColumn();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.CreatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("now()");

                entity.Property(e => e.UpdatedBy).HasDefaultValueSql("'SYSTEM'::character varying");

                entity.HasOne(d => d.Barang)
                    .WithMany(p => p.TbTransaction)
                    .HasForeignKey(d => d.BarangId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbTransaction_TbBarang");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TbTransaction)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TbTransaction_TbCustomer");
            });

            modelBuilder.Entity<TbUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK_User");

                entity.HasIndex(e => e.ActivationLink)
                    .HasName("TbUser_ActivationLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Email)
                    .HasName("TbUser_Email_key")
                    .IsUnique();

                entity.HasIndex(e => e.ForgotPasswordLink)
                    .HasName("TbUser_ForgotPasswordLink_key")
                    .IsUnique();

                entity.HasIndex(e => e.Username)
                    .HasName("TbUser_Username_key")
                    .IsUnique();

                entity.Property(e => e.UserId).UseIdentityAlwaysColumn();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
