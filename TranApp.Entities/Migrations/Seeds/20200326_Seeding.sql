INSERT INTO public."TbBarang"(
	"BarangName", "BarangHarga", "CreatedAt", "CreatedBy")
	VALUES ('Laptop', 1000, now(), 'Sudirman');

INSERT INTO public."TbBarang"(
	"BarangName", "BarangHarga", "CreatedAt", "CreatedBy")
	VALUES ('KeyBoard', 2000, now(), 'Sudirman');
	
INSERT INTO public."TbBarang"(
	"BarangName", "BarangHarga", "CreatedAt", "CreatedBy")
	VALUES ('Mouse', 3000, now(), 'Sudirman');	

INSERT INTO public."TbCustomer"(
	"CustomerName", "CustomerEmail", "CreatedAt", "CreatedBy")
	VALUES ('Thamrin', 'a@b.c', '2020-03-25', 'Rasuna');

INSERT INTO public."TbCustomer"(
	"CustomerName", "CustomerEmail", "CreatedAt", "CreatedBy")
	VALUES ('Said', 'a@b.c', '2020-03-25', 'Rasuna');
	
INSERT INTO public."TbCustomer"(
	"CustomerName", "CustomerEmail", "CreatedAt", "CreatedBy")
	VALUES ('Gatot', 'a@b.c', '2020-03-25', 'Rasuna');
	
INSERT INTO "TbTransaction"(
	"TipeTransaksi", "TransactionDate", "CustomerId", "BarangId", "IsLunas", "TotalTransaction")
	VALUES ('Online', '2020-03-24', 2, 1, '1', 4000);

INSERT INTO "TbTransaction"(
	"TipeTransaksi", "TransactionDate", "CustomerId", "BarangId", "IsLunas", "TotalTransaction")
	VALUES ('Offline', '2020-03-23', 1, 3, '1', 5000);
	
INSERT INTO "TbTransaction"(
	"TipeTransaksi", "TransactionDate", "CustomerId", "BarangId", "IsLunas", "TotalTransaction")
	VALUES ('Online', '2020-03-22', 3, 2, '1', 6000);	