CREATE TABLE "TbUser"
	(
	"UserId"             INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"Username"           VARCHAR (20) NOT NULL UNIQUE,
	"PasswordUser"       VARCHAR(255) NOT NULL,
    "RoleUser"           VARCHAR(20)  NOT NULL,
	"Email"              VARCHAR(255) NOT NULL UNIQUE,
	"IsActive"           BOOLEAN      NOT NULL DEFAULT FALSE,
	"ActivationLink"     VARCHAR(255) NOT NULL UNIQUE,
	"ForgotPasswordLink" VARCHAR(255) UNIQUE,
	CONSTRAINT "PK_User" PRIMARY KEY ("UserId")
	);

CREATE TABLE "TbProduct"
	(
	"ProductId"   INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"ProductName" VARCHAR (255) NOT NULL,
	"Price"       DECIMAL NOT NULL,
	"Stock"       INT NOT NULL,
	CONSTRAINT "PK_Product" PRIMARY KEY ("ProductId")
	);

CREATE TABLE "TbPurchase"
	(
	"PurchaseId"   INT GENERATED ALWAYS AS IDENTITY NOT NULL,
	"UserId"       INT NOT NULL,
	"ProductId"    INT NOT NULL,
	"Quantity"     INT NOT NULL,
	"PurchaseDate" TIMESTAMPTZ NOT NULL,
	CONSTRAINT "PK_Purchase" PRIMARY KEY ("PurchaseId"),
	CONSTRAINT "FK_Purchase_User" FOREIGN KEY ("UserId") REFERENCES "TbUser" ("UserId"),
	CONSTRAINT "FK_Purchase_Product" FOREIGN KEY ("ProductId") REFERENCES "TbProduct" ("ProductId")
	);