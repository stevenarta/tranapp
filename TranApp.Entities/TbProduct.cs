﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranApp.Entities
{
    public partial class TbProduct
    {
        public TbProduct()
        {
            TbPurchase = new HashSet<TbPurchase>();
        }

        [Key]
        public int ProductId { get; set; }
        [Required]
        [StringLength(255)]
        public string ProductName { get; set; }
        [Column(TypeName = "numeric")]
        public decimal Price { get; set; }
        public int Stock { get; set; }

        [InverseProperty("Product")]
        public virtual ICollection<TbPurchase> TbPurchase { get; set; }
    }
}
