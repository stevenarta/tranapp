﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;

namespace TranApp.Entities
{
    public partial class TbTransaction
    {
        [Key]
        public int TransactionId { get; set; }
        [Required]
        [StringLength(255)]
        public string TipeTransaksi { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTimeOffset TransactionDate { get; set; }
        public int CustomerId { get; set; }
        public int BarangId { get; set; }
        [Required]
        [Column(TypeName = "bit(1)")]
        public BitArray IsLunas { get; set; }
        [Column(TypeName = "numeric(18,2)")]
        public decimal? TotalTransaction { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTimeOffset CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTimeOffset UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [ForeignKey(nameof(BarangId))]
        [InverseProperty(nameof(TbBarang.TbTransaction))]
        public virtual TbBarang Barang { get; set; }
        [ForeignKey(nameof(CustomerId))]
        [InverseProperty(nameof(TbCustomer.TbTransaction))]
        public virtual TbCustomer Customer { get; set; }
    }
}
