﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using TranApp.Entities;
using TranApp.Enums;
using TranApp.Models;

namespace TranApp.Services
{
    public class TranAppService
    {
        private readonly TranAppDbContext _tranAppDbContext;
        private readonly IDistributedCache _cacheMan;
        private readonly string _cacheKey = "Purchases";
        public List<PurchaseCartModel> Purchases = new List<PurchaseCartModel>();
        public List<PurchaseViewModel> ListCart = new List<PurchaseViewModel>();
        public bool IsLoaded = false;

        /// <summary>
        /// dependency injection
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="distributedCache"></param>
        public TranAppService(TranAppDbContext dbContext, IDistributedCache distributedCache)
        {
            this._tranAppDbContext = dbContext;
            this._cacheMan = distributedCache;
        }

        /// <summary>
        /// menampilkan semua data transaksi
        /// </summary>
        /// <returns></returns>
        public async Task<List<TransactionViewModel>> GetAllTran()
        {
            var query = from t in _tranAppDbContext.TbTransaction
                        join b in _tranAppDbContext.TbBarang
                        on t.BarangId equals b.BarangId
                        join c in _tranAppDbContext.TbCustomer
                        on t.CustomerId equals c.CustomerId
                        select new TransactionViewModel
                        {
                            TransactionId = t.TransactionId,
                            CustomerName = c.CustomerName,
                            BarangName = b.BarangName,
                            TransactionDate = t.TransactionDate
                        };
            var data = await query
                .AsNoTracking()
                .ToListAsync();
            return data;
        }

        /// <summary>
        /// mendapatkan data produk berdasarkan id
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<ProductViewModel> GetProductDataById(int productId)
        {
            var getProduct = await _tranAppDbContext.TbProduct
                .Where(Q => Q.ProductId == productId)
                .Select(Q => new ProductViewModel
                {
                    ProductName = Q.ProductName,
                    Price = Q.Price,
                    Stock = Q.Stock
                })
                .FirstOrDefaultAsync();

            return getProduct;
        }

        /// <summary>
        /// mendapatkan data dari DistributedCache berdasarkan id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task FetchAllById(int userId)
        {
            if (this.IsLoaded)
            {
                return;
            }

            var all = await _cacheMan.GetStringAsync(this._cacheKey);
            this.IsLoaded = true;
            if (all == null)
            {
                return;
            }

            this.Purchases = JsonSerializer.Deserialize<List<PurchaseCartModel>>(all); //1

            //this.Purchases = JsonConvert.DeserializeObject<List<PurchaseCartModel>>(all);

            this.Purchases = this.Purchases
                .Where(Q => Q.UserId == userId)
                .ToList();
        }

        /// <summary>
        /// menyimpan ke DistributedCache
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private async Task SaveToCache(int userId)
        {
            await FetchAllById(userId);
            //var serialized = JsonConvert.SerializeObject(this.Purchases);
            var serialized = JsonSerializer.Serialize(this.Purchases); //{ProductId:1}
            await _cacheMan.SetStringAsync(this._cacheKey, serialized);
        }

        /// <summary>
        /// menyimpan data ke cart / keranjang belanja
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task InsertCart(PurchaseViewModel model)
        {
            var getProduct = await GetProductDataById(model.ProductId);

            await FetchAllById(model.UserId);

            this.Purchases.Add(new PurchaseCartModel
            {
                UserId = model.UserId,
                ProductId = model.ProductId,
                ProductName = getProduct.ProductName,
                PurchaseDate = DateTimeOffset.UtcNow,
                Quantity = model.Quantity
            });

            await SaveToCache(model.ProductId);
        }

        /// <summary>
        /// mendapatkan data login berdasarkan username yg login
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task<LoginDbModel> GetLogin(string username)
        {
            var getData = await this._tranAppDbContext.TbUser
                .Where(Q => Q.Username == username)
                .Select(Q => new LoginDbModel
                {
                    UserId = Q.UserId,
                    Username = Q.Username,
                    PasswordUser = Q.PasswordUser,
                    RoleUser = Q.RoleUser,
                    Email = Q.Email,
                    IsActive = Q.IsActive
                }).FirstOrDefaultAsync();
            return getData;
        }

        /// <summary>
        /// Fungsi u/ verify password BCrypt
        /// </summary>
        /// <param name="notHash">password asli</param>
        /// <param name="hash">password encrypt</param>
        /// <returns></returns>
        public bool Verify(string notHash, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(notHash, hash);
        }

        /// <summary>
        /// menampilkan semua produk
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductViewModel>> GetAllProduct()
        {
            var getProduct = await _tranAppDbContext.TbProduct
                .Select(Q => new ProductViewModel
                {
                    ProductId = Q.ProductId,
                    ProductName = Q.ProductName,
                    Price = Q.Price,
                    Stock = Q.Stock
                }).ToListAsync();

            return getProduct;
        }

        /// <summary>
        /// mengecek apakah produk yg ingin dibeli sudah ada di keranjang belanja
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CheckCartProductExist(PurchaseViewModel model)
        {
            await FetchAllById(model.UserId);

            var productExist = Purchases.Where(Q => Q.ProductId == model.ProductId).FirstOrDefault();

            if (productExist != null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// mengecek stok masih tersedia atau tidak
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CheckStock(PurchaseViewModel model)
        {
            var getProduct = await GetProductDataById(model.ProductId);

            if (model.Quantity > getProduct.Stock)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Ambil dari Cart (in memory distributed cache), lalu simpan ke ViewModel
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<PurchaseViewModel> GetPurchaseByProductId(int productId, int userId)
        {
            await FetchAllById(userId);

            var findProduct = Purchases
                .Where(Q => Q.ProductId == productId)
                .FirstOrDefault();

            if (findProduct == null)
            {
                return new PurchaseViewModel();
            }

            var result = new PurchaseViewModel()
            {
                UserId = findProduct.UserId,
                ProductId = findProduct.ProductId,
                ProductName = findProduct.ProductName,
                Quantity = findProduct.Quantity
            };

            return result;
        }

        /// <summary>
        /// Edit keranjang belanja
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task UpdateCart(PurchaseViewModel model)
        {
            await FetchAllById(model.UserId);
            var findProduct = Purchases
                .Where(Q => Q.ProductId == model.ProductId)
                .FirstOrDefault();
            findProduct.Quantity = model.Quantity;
            await SaveToCache(model.UserId);
        }

        /// <summary>
        /// menghapus produk tertentu dari keranjang belanja
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task DeleteCart(int productId, int userId)
        {
            await FetchAllById(userId);

            var deletePurchase = Purchases
                .Where(Q => Q.ProductId == productId)
                .FirstOrDefault();

            this.Purchases.Remove(deletePurchase);

            await SaveToCache(userId);
        }

        /// <summary>
        /// order
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> InsertOrder(PurchaseViewModel model)
        //public async Task<bool> InsertOrder(List<PurchaseViewModel> model)
        {
            this._tranAppDbContext.TbPurchase.Add(new TbPurchase
            {
                UserId = model.UserId,
                ProductId = model.ProductId,
                Quantity = model.Quantity,
                PurchaseDate = model.PurchaseDate
            });
            return true;
        }

        public async Task<bool> SaveToDatabase()
        {
            await this._tranAppDbContext.SaveChangesAsync();
            return true;
        }

        public async Task<bool> CheckExistUsername(string username)
        {
            var checkExist = await _tranAppDbContext.TbUser
                            .Where(Q => Q.Username == username)
                            .CountAsync();
            if (checkExist > 0)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> CheckExistEmail(string email)
        {
            var checkExist = await _tranAppDbContext.TbUser
                            .Where(Q => Q.Email == email)
                            .CountAsync();
            if (checkExist > 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Hash Password using BCrypt
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public string Hash(string password)
        {
            //return BCrypt.Net.BCrypt.HashPassword(password, 12);
            return BCrypt.Net.BCrypt.HashPassword(password, 12);
        }

        public async Task<bool> InsertUser(UserViewModel model)
        {
            var activationLink = Hash(model.Username).Replace("/", "");
            activationLink = activationLink.Replace(".", "");

            this._tranAppDbContext.TbUser.Add(new TbUser
            {
                Username = model.Username,
                PasswordUser = Hash(model.PasswordUser),
                RoleUser = UserRoles.User,
                Email = model.Email,
                IsActive = true, //false,
                ActivationLink = activationLink
            });

            await _tranAppDbContext.SaveChangesAsync();
            //await SendMail(PurposeEnum.ActivateUser, model.Email, activationLink);
            return true;
        }
    }
}
