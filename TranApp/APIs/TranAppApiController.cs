﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TranApp.Models;
using TranApp.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TranApp.APIs
{
    [Route("api/v1/tran-app")]
    [ApiController]
    public class TranAppApiController : Controller
    {
        private readonly TranAppService _tranAppMan;

        public TranAppApiController(TranAppService tranAppService)
        {
            this._tranAppMan = tranAppService;
        }

        [HttpGet("get-all-transaction", Name = "getAllTransaction")]
        public async Task<ActionResult<List<TransactionViewModel>>> GetAllTransactionApi()
        {
            var data = await _tranAppMan.GetAllTran();

            return Ok(data);
        }
    }
}
