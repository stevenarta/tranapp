using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;
using TranApp.Models;
using TranApp.Services;

namespace TranApp.Pages.Purchase
{
    [Authorize(Roles = UserRoles.Admin + ", " + UserRoles.User)]
    public class IndexModel : PageModel
    {
        private readonly TranAppService _tranAppMan;
        private readonly IDataProtector _protector;

        public List<PurchaseCartModel> Purchases { get; set; }

        public PurchaseViewModel Pesanan { get; set; }
        [TempData]
        public string SuccessMessage { get; set; }

        public IndexModel(TranAppService tranAppService, IDataProtectionProvider provider)
        {
            this._tranAppMan = tranAppService;
            this._protector = provider.CreateProtector("TranAppProtector");
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var findLogin = await this._tranAppMan.GetLogin(userNameLogin);

            await this._tranAppMan.FetchAllById(findLogin.UserId);
            this.Purchases = this._tranAppMan.Purchases;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var findLogin = await this._tranAppMan.GetLogin(userNameLogin);
            await this._tranAppMan.FetchAllById(findLogin.UserId);
            this.Purchases = this._tranAppMan.Purchases;

            var tanggalSekarang = DateTimeOffset.Now;

            foreach (var item in this.Purchases)
            {
                Pesanan = new PurchaseViewModel()
                {
                    UserId = item.UserId,
                    ProductId = item.ProductId,
                    ProductName = item.ProductName,
                    Quantity = item.Quantity,
                    PurchaseDate = tanggalSekarang
                };

                var result = await this._tranAppMan.InsertOrder(Pesanan);
                if (result == false)
                {
                    SuccessMessage = "Failed";
                    return Page();
                }

            };

            await this._tranAppMan.SaveToDatabase();

            return Page();
        }

        public IActionResult OnPostEdit(int UnProtected)
        {
            string tempUnProtected = UnProtected.ToString();
            string ProtectedData = _protector.Protect(tempUnProtected);

            return Redirect($"~/Purchase/UpdatePurchase/{ProtectedData}");
        }

        public IActionResult OnPostDelete(int UnProtected)
        {
            string tempUnProtected = UnProtected.ToString();
            string ProtectedData = _protector.Protect(tempUnProtected);

            return Redirect($"~/Purchase/DeletePurchase/{ProtectedData}");
        }
    }
}