using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;
using TranApp.Models;
using TranApp.Services;

namespace TranApp.Pages.Purchase
{
    [Authorize(Roles = UserRoles.Admin + ", " + UserRoles.User)]
    public class InsertPurchaseModel : PageModel
    {
        private readonly TranAppService _tranAppMan;

        [BindProperty(SupportsGet = true)]
        public PurchaseViewModel Form { get; set; }

        [BindProperty(SupportsGet = true)]
        public List<ProductViewModel> ListProduct { get; set; }

        [TempData]
        public string SuccessMessage { get; set; }

        public InsertPurchaseModel(TranAppService tranAppService)
        {
            this._tranAppMan = tranAppService;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            ListProduct = await _tranAppMan.GetAllProduct();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var findLogin = await this._tranAppMan.GetLogin(userNameLogin);
            Form.UserId = findLogin.UserId;

            var checkProductExist = await _tranAppMan.CheckCartProductExist(Form);

            if (checkProductExist == false)
            {
                SuccessMessage = "Produk telah ada di keranjang belanja";
                return RedirectToPage();
            }

            var stock = await _tranAppMan.CheckStock(Form);

            if (stock == false)
            {
                SuccessMessage = "Permintaan melebihi Stock, mohon kurangi jumlah pembelian (Quantity) Anda";
                return RedirectToPage();
            }
            else
            {
                await _tranAppMan.InsertCart(Form);
            }


            return Redirect("/Purchase/Index");
        }
    }
}
