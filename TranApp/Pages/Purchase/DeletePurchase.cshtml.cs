using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;
using TranApp.Models;
using TranApp.Services;

namespace TranApp.Pages.Purchase
{
    [Authorize(Roles = UserRoles.Admin + ", " + UserRoles.User)]
    public class DeletePurchaseModel : PageModel
    {
        private readonly TranAppService _tranAppMan;
        private readonly IDataProtector _protector;

        public DeletePurchaseModel(TranAppService tranAppService, IDataProtectionProvider provider)
        {
            this._tranAppMan = tranAppService;
            this._protector = provider.CreateProtector("TranAppProtector");
        }

        [BindProperty(SupportsGet = true)]
        public string ProtectedData { get; set; }

        [BindProperty(SupportsGet = true)]
        public PurchaseViewModel Form { set; get; }

        public async Task<ActionResult> OnGetAsync()
        {
            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userIdLogin = (await this._tranAppMan.GetLogin(userNameLogin)).UserId;

            Form.ProductId = Convert.ToInt32(_protector.Unprotect(ProtectedData));

            Form = await _tranAppMan.GetPurchaseByProductId(Form.ProductId, userIdLogin);

            if (Form == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userIdLogin = (await this._tranAppMan.GetLogin(userNameLogin)).UserId;

            Form = await _tranAppMan.GetPurchaseByProductId(Form.ProductId, userIdLogin);

            await _tranAppMan.DeleteCart(Form.ProductId, Form.UserId);
            return RedirectToPage("/Purchase/Index");
        }
    }
}
