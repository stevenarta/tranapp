using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;
using TranApp.Models;
using TranApp.Services;

namespace TranApp.Pages.Purchase
{
    [Authorize(Roles = UserRoles.Admin + ", " + UserRoles.User)]
    public class UpdatePurchaseModel : PageModel
    {
        private readonly TranAppService _tranAppMan;
        private readonly IDataProtector _protector;

        [BindProperty(SupportsGet = true)]
        public string ProtectedData { get; set; }

        public UpdatePurchaseModel(TranAppService tranAppService, IDataProtectionProvider provider)
        {
            this._tranAppMan = tranAppService;
            this._protector = provider.CreateProtector("TranAppProtector");
        }
        [BindProperty(SupportsGet = true)]
        public PurchaseViewModel Form { set; get; }


        public async Task<IActionResult> OnGetAsync()
        {
            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userIdLogin = (await this._tranAppMan.GetLogin(userNameLogin)).UserId;

            Form.ProductId = Convert.ToInt32(_protector.Unprotect(ProtectedData));

            Form = await _tranAppMan.GetPurchaseByProductId(Form.ProductId, userIdLogin);

            if (Form == null)
            {
                return NotFound();
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return BadRequest(ModelState);
            }
            var userNameLogin = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var userIdLogin = (await this._tranAppMan.GetLogin(userNameLogin)).UserId;

            Form = new PurchaseViewModel()
            {
                UserId = userIdLogin,
                ProductId = Form.ProductId,
                Quantity = Form.Quantity
            };

            await _tranAppMan.UpdateCart(Form);
            return RedirectToPage("/Purchase/Index");
        }
    }
}
