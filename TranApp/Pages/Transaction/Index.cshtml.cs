using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;

namespace TranApp.Pages.Transaction
{
    [Authorize(Roles = UserRoles.User + ", " + UserRoles.Admin)]
    public class IndexModel : PageModel
    {
        public void OnGet()
        {
        }
    }
}
