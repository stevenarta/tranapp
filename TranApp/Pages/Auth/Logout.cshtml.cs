using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TranApp.Enums;

namespace TranApp.Pages.Auth
{
    [Authorize(Roles = UserRoles.Admin + ", " + UserRoles.User)]
    public class LogoutModel : PageModel
    {
        public async Task<IActionResult> OnGet()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync(TranAppAuthenticationSchemes.Cookie);
            }
            return Redirect("~/Auth/Login");
        }
    }
}
